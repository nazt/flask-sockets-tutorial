# source this file, don't execute it
# ----------------------------------

virtualenv --distribute venv
. venv/bin/activate
pip install -r requirements.txt
